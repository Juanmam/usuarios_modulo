// Requires
const express = require('express');
const router = express.Router();

// Models
const modelController = require('../controllers/admin-controller');

////////////////////API////////////////////

//
router.post('/create', modelController.register);

//
router.get('/listAll', modelController.read);

//
router.get('/login', modelController.login);

//
router.delete('/delete', modelController.delete);

// 
router.get('/rankUp', modelController.rankUp);

module.exports = router;
