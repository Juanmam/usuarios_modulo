// Requires
const express = require('express');
const router = express.Router();

// Models
const modelController = require('../controllers/user-controller');

////////////////////API////////////////////

// createUser
router.post('/create', modelController.create);

// listUsers
router.get('/', modelController.list);

// findUserByName: returns all users with the given name
router.get('/findName', modelController.findByName);

// findUserById: returns the user with the given id
router.get('/findId', modelController.findById);

// findUserByNIN: returns the user with the given National Identification Number
router.get('/findNIN', modelController.findByNIN)

// updateById: Updates value given an id and values to change.
router.put('/updateById', modelController.updateById)

// updateByNIN: Updates value given a National Identification Number.
router.put('/updateByNIN', modelController. updateByNIN)

// deletesId: deletes by Id
router.delete('/deleteById', modelController.deleteById);

// deletesNIN: deletes by National Identification Number
router.delete('/deleteByNIN', modelController.deleteByNIN);

// deleteAll: deletes ALL users
router.delete('/deleteAll', modelController.deleteAll);

// login: validates using email and password
router.get('/login', modelController.login);

// test
router.get('/test', modelController.test);

module.exports = router;
