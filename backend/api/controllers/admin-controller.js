///REQUIRES
const mongoose = require('mongoose');
const bcrypt   = require('bcrypt');
const scrypt   = require('scrypt');

///MODELS
const Admin = require('../models/admin-model');
const User  = require('../models/user-model');

//// API

exports.register = (req, res, next) => {
  let data = JSON.parse(req.body.data);

  console.log(typeof scrypt)
  for (var prop in scrypt) {
    console.log(prop + ' ' + scrypt[prop])
  }

  // Check if instance already exists.

  Admin.find({}).then(result => {
    // Check if the query is returns empty
    if(result.length == 0){

      // We encrypt the password here using scrypt base 16.
      // The base can be altered to make it even more secure, like 64 or higher.
      let key = new Buffer(data.password);

      scrypt.hash(key, { "N":16, "r":1, "p":1 },64,"").then(result => {
        let pass = result.toString("hex");

        // Create new admin
        let newAdmin = new Admin({
          name: data.name,
          password: pass
        });

        newAdmin.save().then(result => {
          res.status(200).json({
            result
          });
        }).catch(err => {
          res.status(500).json({
            err
          });
        });
      }).catch(err => {
        res.status(500).json({err});
      });
    } else {
      res.status(200).json({
        result
      });
    }
  }).catch(err => {
    res.status(500).json({
      err
    });
  })
}

exports.read = (req, res, next) => {
  Admin.find({}).then(result => {
    res.status(200).json({
      result
    });
  }).catch(err => {
    res.status(500).json({
      err
    });
  });
}

exports.delete = (req, res, next) => {
  Admin.deleteMany().then(result => {
    res.status(200).json({
      result
    });
  }).catch(err => {
    res.status(500).json({
      err
    });
  });
}

exports.login = (req, res, next) => {
  let data = JSON.parse(req.body.data);
  // Request variables
  let name = data.name;

  // We encrypt the password here using scrypt to compare it.
  let key = new Buffer(data.password);
  let pass = "";

  scrypt.hash(key, { "N":16, "r":1, "p":1 },64,"").then(result => {
    pass = result.toString("hex");

    Admin.find({}).then(result => {

      let admin_name = result[0].name
      let admin_pass = result[0].password

      if(admin_name === name && admin_pass === pass){
        res.status(200).json({
          result
        });
      }

      res.status(401).json({
        message: "Wrong name or password."
      });
    }).catch(err => {
      res.status(500).json({
        err
      });
    });

  }).catch(err => {
    res.status(500).json({err});
  });
}

exports.rankUp = (req, res, next) => {
  let data = JSON.parse(req.body.data);
  let rank = Math.floor(Number(data.ranking));

  if(rank !== Infinity && String(rank) === data.ranking && rank >= 0){
    // The rank is a positive integer, so it can be set correctly.
    // Time to find the user by its doctype and newAdmin
    User.findOneAndUpdate({ "documentType" : data.documentType, "nin" : data.nin }, { "ranking" : String(rank) })
    .then(result => {
      res.status(200).json({
        result
      });
    }).catch(err => {
      res.status(500).json({
        err
      });
    });
  } else {
    // Some problem with the rank format probably.
    result.status(200).json({
      message: "Couldn't parse the rank into an actual usable integer."
    })
  }
}
