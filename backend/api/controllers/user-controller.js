///REQUIRES
const mongoose    = require('mongoose');
const bcrypt      = require('bcrypt');
const scrypt      = require('scrypt');
const crypto      = require('crypto');
const config      = require('../../config/config');
const request     = require('request')
const querystring = require('querystring')

function hash(password){

  let url  = securityURL + ':' + securityPort + '/api/hashing/hash'

  /*let data = querystring.stringify({
    data: [{
      pass: password
    }]
  })*/
  let data = querystring.stringify({
    pass: password
  })

  console.log('input ' + password);
  console.log('initial data ' + data);

  request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    form:{
      pass: password
    },
    uri: url,
    method: 'POST'
  }, function (err, res, body) {
    console.log(err)
    console.log(res);
    console.log(body);
  });
/*
  request.post(url, {form:{pass: password}},
    (error, res, body) => {
  if (error) {
    console.error(error)
    return
  }
  console.log(`statusCode: ${res.statusCode}`)
  console.log(body)

})*/
}

///MODELS
const User = require('../models/user-model');

function print(text){
  console.log(text)
}

//// API

// Create: Creates the user using the request given.
// Returns the creation message and result or error.
exports.create = (req, res, next) => {
  // Parse request into a JSON object.
  let data = JSON.parse(req.body.data);

  // Get password hashed.


  let pass = result.toString("hex");

  let nin = encrypt(data.nin)
  let addr = encrypt(data.address)
  let email = encrypt(data.email)
  let phone = encrypt(data.phone)
  let cellphone = encrypt(data.cellphone)
  let company = encrypt(data.company)
  let position = encrypt(data.position)
  let profileURL = encrypt(data.profilePicURL)
  let nit = encrypt(data.nit)

  User.find({ "documentType": data.documentType , "nin": data.nin })
  .then(result => {
    if(result === null || result.length === 0 || result === undefined){
      // The user does not exist and we can create it.
      // Lets create a new user with the given data.
      let newUser = new User({
        firstName:     data.firstName,
        lastName:      data.lastName,
        password:      pass,
        documentType:  data.documentType,
        nin:           nin.encryptedData,
        ranking:       data.ranking,
        gender:        data.gender,
        address:       addr.encryptedData,
        city:          data.city,
        state:         data.state,
        country:       data.country,
        email:         email.encryptedData,
        phone:         phone.encryptedData,
        cellphone:     cellphone.encryptedData,
        birthDate:     data.birthDate,
        company:       company.encryptedData,
        department:    data.department,
        position:      position.encryptedData,
        profilePicURL: profileURL.encryptedData,
        nit:           nit.encryptedData
      });

      // Save the object into mongoose
      newUser.save().then(result => {
        res.status(200).json({
          message: 'User successfully created.',
          result
        });
      }).catch(err => {
        res.status(500).json({
          error: err,
          message: "User couldn't be created."
        });
      });

    } else {
      // The user already exists and we will return an error status
      message_data = "User already exists with document ";
      message_data.concat(data.documentType, ' ', data.nin)
      res.status(200).json({
        message: message_data
      });
    }
  }).catch(err => {
    res.status(500).json({
      message: "Something went wrong while creating user.",
      error: err
    });
  });
}

// List: Lists ALL users.
// Returns an ARRAY of USERS.
exports.list = (req, res, next) => {
  decrypted = []
  User.find({})
  .then(result => {
    // Decrypt all encrypted data except for password

    console.log("-------------------------------")

    result.forEach(user => {
      decrypted.push({
        firstName: user.firstName,
        lastName: user.lastName,
        documentType: user.documentType,
        nin: decrypt({
          iv: user.ninkey,
          encryptedData: user.nin
        }),
        address: decrypt({
          iv: user.addresskey,
          encryptedData: user.address
        }),
        city: user.city,
        state: user.state,
        country: user.country,
        email: decrypt({
          iv: user.emailkey,
          encryptedData: user.email
        }),
        phone: decrypt({
          iv: user.phonekey,
          encryptedData: user.phone
        }),
        cellphone: decrypt({
          iv: user.cellphonekey,
          encryptedData: user.cellphone
        }),
        birthDate: user.birthDate,
        company: decrypt({
          iv: user.companykey,
          encryptedData: user.company
        }),
        department: user.department,
        position: decrypt({
          iv: user.positionkey,
          encryptedData: user.position
        }),
        profilePicURL: decrypt({
          iv: user.profilePicURLkey,
          encryptedData: user.profilePicURL
        }),
        nit: decrypt({
          iv: user.nitkey,
          encryptedData: user.nit
        })
      });
    });

    res.status(200).json({
      message: "Listed users successfully.",
      decrypted,
      result
    });
  }).catch(err => {
    res.status(500).json({
      error: err,
      message: "An error ocurred while listing the users."
    });
  });
}

// findByName: Finds the users by names.
// Returns an ARRAY of USERS.
exports.findByName = (req, res, next) => {
  let data = JSON.parse(req.body.data);

  User.findOne({"name": data.name})
  .then(result => {
    res.status(200).json({
      message: "Listed user successfully.",
      result
    });
  }).catch(err => {
    res.status(500).json({
      error: err,
      message: "An error ocurred while listing the user."
    });
  });
}

// findById: returns a user given an ID.
// Returns a SINGLE USER.
exports.findById = (req, res, next) => {
  let data = JSON.parse(req.body.data)

  User.findOne({"_id": data.id})
  .then(result => {
    res.status(200).json({
      message: "Listed user successfully.",
      result
    });
  }).catch(err => {
    res.status(500).json({
      error: err,
      message: "An error ocurred while listing the user."
    });
  });
}

// findByNIN: returns a user given an NIN.
// Returns a SINGLE USER.
exports.findByNIN = (req, res, next) => {
  let data = JSON.parse(req.body.data);

  User.findOne({"nin": data.nin})
  .then(result => {
    res.status(200).json({
      message: "Listed user successfully.",
      result
    });
  }).catch(err => {
    res.status(500).json({
      error: err,
      message: "An error ocurred while listing the user."
    });
  });
}

// updateById: updates a user by its ID
exports.updateById = (req, res, next) => {
  let data = JSON.parse(req.body.data);
  let id = data.id;
  delete data.id;

  User.findOneAndUpdate({ "_id": id }, data)
  .then(result => {
    res.status(200).json({
      message: "Update successfull.",
      result
    });
  }).catch(err => {
    res.status(500).json({
      message: "Couldn't update the user.",
      err
    });
  });
}

// updateByNIN: updates a user by its National Identification Number
// Note that you can't update the nin using this method.
exports.updateByNIN = (req, res, next) => {
  let data = JSON.parse(req.body.data);

  User.findOneAndUpdate({ "nin": data.nin }, data)
  .then(result => {
    res.status(200).json({
      message: "Update successfull.",
      result
    });
  }).catch(err => {
    res.status(500).json({
      message: "Couldn't update the user.",
      err
    });
  });
}

// deleteId: deletes a SINGLE user by its ID.
exports.deleteById = (req, res, next) => {
  let data = JSON.parse(req.body.data);

  User.findOneAndDelete({"_id": data.id})
  .then(result => {
    res.status(200).json({
      message: "User deleted.",
      result
    });
  }).catch(err => {
    res.status(500).json({
      message: "An error ocurred while trying to delete the user.",
      err
    });
  });
}

// deleteNIN: deletes a SINGLE user by it National Identification Number
exports.deleteByNIN = (req, res, next) => {
  let data = JSON.parse(req.body.data);

  User.findOneAndDelete({ "nin": data.nin }).then(result => {
    res.status(200).json({
      message: "User deleted.",
      result
    });
  }).catch(err => {
    res.status(500).json({
      message: "An error ocurred while trying to delete the user.",
      err
    });
  });
}

// deleteAll: deletes ALL the users.
exports.deleteAll = (req, res, next) => {
  User.deleteMany().then(result => {
    res.status(200).json({
      message: "Users deleted.",
      result
    })
  }).catch(err => {
    res.status(500).json({
      message: "An error ocurred while trying to delete the users.",
      err
    })
  });
}

// login: Takes the user email and password and validates the data.
exports.login = (req, res, next) => {
  let data = JSON.parse(req.body.data);

  // We encrypt the password here using scrypt.
  // We use the encrypted key to compare and validate the login.
  let key = new Buffer(data.password);

  scrypt.hash(key, { "N":16, "r":1, "p":1 },64,"").then(result => {
    let pass = result.toString("hex");

    User.findOne({ "email" : data.email }).then(result => {
      // Validate user password
      if(result.password === pass){
        res.status(200).json({
          login: true,
          result
        });
      } else {
        res.status(401).json({
          login: false,
          message: "Incorrect email or password."
        });
      }
    }).catch(err => {
      res.status(500).json({
        err
      });
    });
  }).catch(err =>{
    res.status(500).json({
      err
    });
  });
}

exports.test = (req, res, next) => {
  console.log(hash("userpassword"))
}
