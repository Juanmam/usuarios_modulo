const mongoose = require('mongoose');

defaultPic = "https://pbs.twimg.com/profile_images/518089855671074816/S_BpQudi_400x400.png";

const userSchema = mongoose.Schema({
  firstName: { type: String, require: true },
  lastName: { type: String, require: true },
  password: { type: String, require: true },
  documentType: { type: String, require: false },
  // NIN: National Identification Number.
  nin: { type: String, require: false },
  ranking: { type: Number, require: false, default: 0},
  gender: { type: String, require: false },
  address: { type: String, require: false },
  city: { type: String, require: false },
  state: { type: String, require: false },
  country: { type: String, require: false },
  email: { type: String, require: false },
  phone: { type: String, require: false },
  cellphone: { type: String, require: false },
  birthDate: { type: String, require: false },
  company: { type: String, require: false },
  department: { type: String, require: false },
  position: { type: String, require: false },
  profilePicURL: { type: String, require: false, default: defaultPic},
  nit: { type: String, require: false }
});

module.exports = mongoose.model('User', userSchema);
